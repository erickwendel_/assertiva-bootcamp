#!/usr/bin/env node
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Commander = require("commander");
//Importamos a lib para manipulação de funcoes assincronas
var Bluebird = require("bluebird");
//importamos a lib para manipulaÇao de arquivo
var Fs = require("fs");
// convertemos todas as funcoes de manipulacao
//de arquivo, para PROMISES
var fs = Bluebird.promisifyAll(Fs);
// Minha entidade
var Pessoa = /** @class */ (function () {
    function Pessoa() {
    }
    return Pessoa;
}());
var Runner = /** @class */ (function () {
    function Runner() {
    }
    //retorna uma lista de pessoas do arquivo
    //para resolver o problema da PROMISE NAO EXISTIR
    //adicionamos no tsconfig.json o objeto lib
    Runner.obterDados = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result2, result, pessoa;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result2 = fs.existsSync(Runner.NOME_ARQUIVO);
                        if (!!result2) return [3 /*break*/, 2];
                        return [4 /*yield*/, fs.writeFileAsync(Runner.NOME_ARQUIVO, '[]')];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [4 /*yield*/, fs.readFileAsync(Runner.NOME_ARQUIVO)];
                    case 3:
                        result = _a.sent();
                        pessoa = JSON.parse(result);
                        return [2 /*return*/, pessoa];
                }
            });
        });
    };
    Runner.inserirDados = function (pessoa) {
        return __awaiter(this, void 0, void 0, function () {
            var dados;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Runner.obterDados()];
                    case 1:
                        dados = _a.sent();
                        //adicionamos a nova pessoa
                        dados.push(pessoa);
                        //escrevemos o arquivo com a string completa
                        // a partir do stringify, transformamos em de pessoa para string
                        return [4 /*yield*/, fs.writeFileAsync(Runner.NOME_ARQUIVO, JSON.stringify(dados))];
                    case 2:
                        //escrevemos o arquivo com a string completa
                        // a partir do stringify, transformamos em de pessoa para string
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    Runner.NOME_ARQUIVO = 'BANCO_LOCAL';
    return Runner;
}());
// Runner.obterDados()
//   .then(result => console.log(result))
//   .catch(error => console.error(error));
Commander
    .version('0.1.0')
    .option('-n, --nome [value]', 'Receber um nome')
    .option('-i, --idade [value]', 'Receber um idade')
    .option('-f, --find', 'Procurar uma pessoa')
    .option('-a, --findAll', 'Obter pessoas')
    .option('-u, --update', 'Atualizar uma pessoa')
    .option('-d, --delete', 'Remove uma pessoa')
    .option('-c, --create', 'Cria uma pessoa')
    .parse(process.argv);
function execution() {
    return __awaiter(this, void 0, void 0, function () {
        var nome, idade, dados, dadosFiltrados, dados, dadosFiltrados_1, items, dados, dadosFiltrados, dados, _a, _b, _c, _d;
        return __generator(this, function (_e) {
            switch (_e.label) {
                case 0:
                    nome = Commander.nome;
                    idade = Commander.idade;
                    if (!Commander["delete"]) return [3 /*break*/, 3];
                    return [4 /*yield*/, Runner.obterDados()];
                case 1:
                    dados = _e.sent();
                    dadosFiltrados = dados.filter(function (item) { return item.nome === nome; });
                    console.log('delete', dadosFiltrados);
                    return [4 /*yield*/, fs.writeFileAsync(Runner.NOME_ARQUIVO, JSON.stringify(dadosFiltrados))];
                case 2:
                    _e.sent();
                    return [2 /*return*/];
                case 3:
                    if (!Commander.update) return [3 /*break*/, 6];
                    return [4 /*yield*/, Runner.obterDados()];
                case 4:
                    dados = _e.sent();
                    dadosFiltrados_1 = dados.filter(function (item) { return item.nome.indexOf(nome) != -1; });
                    items = dados.map(function (item) {
                        //filtramos todos os itens ja existentes trazendo, somente
                        //quem estiver com o mesmo nome de quem foi filtrado
                        var resultados = dadosFiltrados_1.filter(function (filtrados) { return filtrados.nome == item.nome; });
                        //se não encontrar ninguem, não tem alteração
                        if (!resultados.length)
                            return item;
                        // se encontrar, altera a idade
                        item.idade = idade;
                        return item;
                    });
                    console.log('update', items);
                    return [4 /*yield*/, fs.writeFileAsync(Runner.NOME_ARQUIVO, JSON.stringify(items))];
                case 5:
                    _e.sent();
                    return [2 /*return*/];
                case 6:
                    if (!Commander.find) return [3 /*break*/, 8];
                    return [4 /*yield*/, Runner.obterDados()];
                case 7:
                    dados = _e.sent();
                    dadosFiltrados = dados.filter(function (item) { return item.nome.indexOf(nome) != -1; });
                    console.log('find', dadosFiltrados);
                    return [2 /*return*/];
                case 8:
                    if (!Commander.findAll) return [3 /*break*/, 10];
                    return [4 /*yield*/, Runner.obterDados()];
                case 9:
                    dados = _e.sent();
                    console.log('findAll', dados);
                    return [2 /*return*/];
                case 10:
                    if (!Commander.create) return [3 /*break*/, 13];
                    return [4 /*yield*/, Runner.inserirDados({
                            nome: nome,
                            idade: idade
                        })];
                case 11:
                    _e.sent();
                    _b = (_a = console).log;
                    _d = (_c = JSON).stringify;
                    return [4 /*yield*/, Runner.obterDados()];
                case 12:
                    _b.apply(_a, [_d.apply(_c, [_e.sent()])]);
                    return [2 /*return*/];
                case 13: return [2 /*return*/];
            }
        });
    });
}
execution()
    .then(function (resultado) { return console.log('resultado', resultado); })["catch"](function (erro) { return console.error(erro); });
//# sourceMappingURL=App.js.map