interface IBase<T> {
  listar(): T[];
  cadastrar(item: T): void;
}

abstract class Base<T> implements IBase<T> {
  public items: T[] = [];
  listar(): T[] {
    return this.items;
  }
  cadastrar(item: T): void {
    this.items.push(item);
  }
}

class Funcionario {
  public nome: string;
}
class Cliente {
  public nome: string;
}

class ServicoFuncionario extends Base<Funcionario> {}
class ServicoCliente extends Base<Cliente> {}

const funcionario = new ServicoFuncionario();
funcionario.cadastrar(<Funcionario>{ nome: 'Erick' });

const cliente = new ServicoCliente();
cliente.cadastrar(<Cliente>{ nome: 'Cliente' });

console.log(`
 Funcionarios: ${JSON.stringify(funcionario.listar())}
 Clientes: ${JSON.stringify(cliente.listar())}
`);
