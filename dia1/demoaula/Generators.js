"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Base = /** @class */ (function () {
    function Base() {
        this.items = [];
    }
    Base.prototype.listar = function () {
        return this.items;
    };
    Base.prototype.cadastrar = function (item) {
        this.items.push(item);
    };
    return Base;
}());
var Funcionario = /** @class */ (function () {
    function Funcionario() {
    }
    return Funcionario;
}());
var Cliente = /** @class */ (function () {
    function Cliente() {
    }
    return Cliente;
}());
var ServicoFuncionario = /** @class */ (function (_super) {
    __extends(ServicoFuncionario, _super);
    function ServicoFuncionario() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ServicoFuncionario;
}(Base));
var ServicoCliente = /** @class */ (function (_super) {
    __extends(ServicoCliente, _super);
    function ServicoCliente() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ServicoCliente;
}(Base));
var funcionario = new ServicoFuncionario();
funcionario.cadastrar({ nome: 'Erick' });
var cliente = new ServicoCliente();
cliente.cadastrar({ nome: 'Cliente' });
console.log("\n Funcionarios: " + JSON.stringify(funcionario.listar()) + "\n Clientes: " + JSON.stringify(cliente.listar()) + "\n");
//# sourceMappingURL=Generators.js.map