#!/usr/bin/env node

// Importamos o modulo
//para trabalhar com linha de comando
import * as Commander from 'commander';

//Importamos a lib para manipulação de funcoes assincronas
import * as Bluebird from 'bluebird';
//importamos a lib para manipulaÇao de arquivo
import * as Fs from 'fs';

// convertemos todas as funcoes de manipulacao
//de arquivo, para PROMISES
const fs: any = Bluebird.promisifyAll(Fs);
// Minha entidade
class Pessoa {
  public nome: string;
  public idade: number;
}
class Runner {
  static NOME_ARQUIVO = 'BANCO_LOCAL';
  //retorna uma lista de pessoas do arquivo
  //para resolver o problema da PROMISE NAO EXISTIR
  //adicionamos no tsconfig.json o objeto lib
  static async obterDados(): Promise<Pessoa[]> {
    //vamos ler o arquivo e retornar os dados
    // estamos lendo um arquivo
    // quando ele conseguir abrir o arquivo, ele chama uma função

    // para depurar a aplicação, precisamos de 2 passos
    // 1-> tsconfig.json -> ativar o source map: true
    // 2-> entramos no arquivo launch.json para verificar o arquivo
    // 3-> Ctrl + Shift + P, digita lauch
    // 4-> Alterar de index.js para o nome do nosso arquivo
    const result2 = fs.existsSync(Runner.NOME_ARQUIVO);
    if (!result2) {
      await fs.writeFileAsync(Runner.NOME_ARQUIVO, '[]');
    }

    const result: string = await fs.readFileAsync(Runner.NOME_ARQUIVO);
    const pessoa = JSON.parse(result) as Pessoa[];
    return pessoa;
  }

  static async inserirDados(pessoa: Pessoa) {
    //carregamos os dados na memoria
    let dados = await Runner.obterDados();
    //adicionamos a nova pessoa
    dados.push(pessoa);
    //escrevemos o arquivo com a string completa
    // a partir do stringify, transformamos em de pessoa para string
    await fs.writeFileAsync(Runner.NOME_ARQUIVO, JSON.stringify(dados));
  }
}

// Runner.obterDados()
//   .then(result => console.log(result))
//   .catch(error => console.error(error));

Commander
  //definimos a versão do nosso modulo
  .version('0.1.0')
  //definimos uma opção para nossa ferramenta
  // quando executarmos o arquivo
  //passando um valor, deve exibir no terminal
  .option('-n, --nome [value]', 'Receber um nome')
  .option('-i, --idade [value]', 'Receber um idade')
  .option('-f, --find', 'Procurar uma pessoa')
  .option('-a, --findAll', 'Obter pessoas')
  .option('-u, --update', 'Atualizar uma pessoa')
  .option('-d, --delete', 'Remove uma pessoa')
  .option('-c, --create', 'Cria uma pessoa')
  .parse(process.argv);

async function execution() {
  const nome = Commander.nome;
  const idade = Commander.idade;

  if (Commander.delete) {
    const dados = await Runner.obterDados();
    //pegamos todos os dados, e trazemos somente quem é do filtro,
    //do update
    const dadosFiltrados = dados.filter(item => item.nome === nome);
    console.log('delete', dadosFiltrados);

    await fs.writeFileAsync(
      Runner.NOME_ARQUIVO,
      JSON.stringify(dadosFiltrados),
    );

    return;
  }

  if (Commander.update) {
    const dados = await Runner.obterDados();
    //pegamos todos os dados, e trazemos somente quem é do filtro,
    //do update
    const dadosFiltrados = dados.filter(item => item.nome.indexOf(nome) != -1);
    //vamos navegar em cada item da lista
    const items = dados.map(item => {
      //filtramos todos os itens ja existentes trazendo, somente
      //quem estiver com o mesmo nome de quem foi filtrado
      const resultados = dadosFiltrados.filter(
        filtrados => filtrados.nome == item.nome,
      );
      //se não encontrar ninguem, não tem alteração
      if (!resultados.length) return item;

      // se encontrar, altera a idade
      item.idade = idade;
      return item;
    });

    console.log('update', items);
    await fs.writeFileAsync(Runner.NOME_ARQUIVO, JSON.stringify(items));
    return;
  }

  if (Commander.find) {
    const dados = await Runner.obterDados();
    const dadosFiltrados = dados.filter(item => item.nome.indexOf(nome) != -1);
    console.log('find', dadosFiltrados);
    return;
  }

  if (Commander.findAll) {
    const dados = await Runner.obterDados();
    console.log('findAll', dados);
    return;
  }

  if (Commander.create) {
    await Runner.inserirDados(<Pessoa>{
      nome,
      idade,
    });
    console.log(JSON.stringify(await Runner.obterDados()));
    return;
  }
}

execution()
  .then(resultado => console.log('resultado', resultado))
  .catch(erro => console.error(erro));
