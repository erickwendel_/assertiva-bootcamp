* Heroku Toolbelt (CLI do Heroku)
* Docker

http://bit.ly/assertiva-bootcamp-aula3

* Adicionar Logs
* Publicação de nossa aplicação

  * [PM2](http://pm2.keymetrics.io)
  * PM2 KeyMetrics

* Adicionar Banco de produção com MongoLab
* Docker
* Adicionar nossa aplicação ao Docker
* Plus => O que querem aprender

# docker run --restart=always -p 33033:27017 --name mongo -v /data/db:/data/db -d mongo

PORT=3000
foldername="${PWD##\*/}"
docker build --build-arg env=development -t "${foldername}" .
docker run -it -p "$PORT":"$PORT" -v "$PWD"/src:/frontend/src "${foldername}"

ROM node:8-alpine

ENV NPM_CONFIG_LOGLEVEL warn
ARG env
ENV ENVIRONMENT $env

ADD . /api-campanha
WORKDIR api-campanha

RUN npm i --silent

CMD if [ ${ENVIRONMENT} = development ]; \
 then \
 npm install -g nodemon && \
 npm run server \
 else \
 npm i -g pm2 --silent \
 pm2-docker app.js; \
 fi

EXPOSE 3002
