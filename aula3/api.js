/*
  Instalamos o PM2 para manipular e monitorar 
  o estado dos servidores 
  npm i -g pm2 
  usamos o "pm2 start --name NOME_DA_APLICACAO" api.js
  listar servicos: pm2 list 
  pm2 monit 0 
  
  rodamos o pm2 -i 10 para rodar 10 instancias de
  nossa aplicação 

  https://app.keymetrics.io/#/

  criamos uma conta no heroku
  baixamos o heroku toolbelt

  heroku login na pasta do artigo
  fizemos um 
    heroku apps:create \ 
      bootcamp-usuarios
*/

//trazemos o Database para a memoria
const Database = require('./index');
//importamos o Joi, para validar nosso objetos
const Joi = require('joi');
//Importamos o Boom, para manipular
//respostas Http
const Boom = require('boom');

//importamos o winston, para
// manipular logs
const Winston = require('winston');
Winston.configure({
  transports: [
    new Winston.transports.File({ filename: 'log.txt' }),
    new Winston.transports.Console(),
  ],
});
// Winston.info('Log de Informação');
// Winston.error('Deu RUIM');

//importamos o Hapi, para manipular rotas
const Hapi = require('hapi');
const app = new Hapi.Server();
//definimos a variavel que virá do heroku
app.connection({ port: process.env.PORT || 3000 });
app.route([
  {
    //definir quem é o caminho
    path: '/usuarios',
    method: 'GET',
    handler: async (req, reply) => {
      try {
        Winston.info(`/usuarios [GET] - ${req.info.remoteAddress}`);
        const db = new Database();

        return reply(await db.list());
      } catch (error) {
        Winston.error(error);
        return reply(Boom.internal('Deu Ruim'));
      }
    },
  },
  {
    path: '/usuarios',
    method: 'POST',
    config: {
      handler: async (req, reply) => {
        try {
          Winston.info(`/usuarios [POST] - ${req.info.remoteAddress}`);

          const { payload } = req;
          const db = new Database();
          return reply(await db.save(payload.nome, payload.idade));
        } catch (error) {
          Winston.error(error);
          return reply(Boom.internal('Deu Ruim'));
        }
      },
      validate: {
        payload: {
          nome: Joi.string()
            .min(3)
            .max(10)
            .required(),

          idade: Joi.number()
            .max(50)
            .min(10),
        },
      },
    },
  },
  {
    path: '/usuarios/{id}',
    method: 'PUT',
    config: {
      handler: async (req, reply) => {
        try {
          Winston.info(`/usuarios [PUT] - ${req.info.remoteAddress}`);

          const { payload } = req;
          const { id } = req.params;

          const db = new Database();
          return reply(
            await db.update(id, {
              nome: payload.nome,
              idade: payload.idade,
            }),
          );
        } catch (error) {
          Winston.error(error);
          return reply(Boom.internal());
        }
      },
      validate: {
        payload: {
          nome: Joi.string()
            .min(3)
            .max(10),

          idade: Joi.number().max(50),
        },
      },
    },
  },
  {
    path: '/usuarios/{id}',
    method: 'DELETE',
    config: {
      handler: async (req, reply) => {
        try {
          Winston.info(`/usuarios [DELETE] - ${req.info.remoteAddress}`);

          const { id } = req.params;

          const db = new Database();
          return reply(await db.delete(id));
        } catch (error) {
          Winston.error(error);
          return reply(Boom.internal('Erro!'));
        }
      },
    },
  },
  {
    path: '/usuarios/{nome}',
    method: 'GET',
    config: {
      handler: async (req, reply) => {
        try {
          throw new Error('ae');
          const { nome } = req.params;
          Winston.info(`/usuarios/${nome} [GET] - ${req.info.remoteAddress}`);
          // nome = NAOEXISTE.NOME;
          const db = new Database();
          return reply(await db.findOne(nome));
        } catch (error) {
          Winston.error(error);
          return reply(Boom.internal());
        }
      },
      validate: {
        params: {
          nome: Joi.string()
            .max(10)
            .min(2)
            .required(),
        },
      },
    },
  },
]);
app.start(() => console.log('servidor no ar!!'));
// const http = require('http');
// http
//   .createServer((req, resp) => {
//     resp.end('Hello world');
//   })
//   .listen(3000, () => {
//     console.log('servidor está ativo!!');
//   });
