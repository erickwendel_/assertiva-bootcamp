//trazemos o Database para a memoria
const Database = require('./index');
//importamos o Joi, para validar nosso objetos
const Joi = require('joi');
//importamos o Hapi, para manipular rotas
const Hapi = require('hapi');
const app = new Hapi.Server();
app.connection({ port: 3000 });
app.route([
  {
    //definir quem é o caminho
    path: '/usuarios',
    method: 'GET',
    handler: (req, reply) => {
      const db = new Database();
      return reply(db.list());
    },
  },
  {
    path: '/usuarios',
    method: 'POST',
    config: {
      handler: (req, reply) => {
        const { payload } = req;
        const db = new Database();
        return reply(db.save(payload.nome, payload.idade));
      },
      validate: {
        payload: {
          nome: Joi.string()
            .min(3)
            .max(10)
            .required(),

          idade: Joi.number()
            .max(50)
            .min(10),
        },
      },
    },
  },
  {
    path: '/usuarios/{id}',
    method: 'PUT',
    config: {
      handler: (req, reply) => {
        const { payload } = req;
        const { id } = req.params;

        const db = new Database();
        return reply(
          db.update(id, {
            nome: payload.nome,
            idade: payload.idade,
          }),
        );
      },
      validate: {
        payload: {
          nome: Joi.string()
            .min(3)
            .max(10),

          idade: Joi.number().max(50),
        },
      },
    },
  },
  {
    path: '/usuarios/{id}',
    method: 'DELETE',
    config: {
      handler: (req, reply) => {
        const { id } = req.params;

        const db = new Database();
        return reply(db.delete(id));
      },
    },
  },
  {
    path: '/usuarios/{nome}',
    method: 'GET',
    config: {
      handler: async (req, reply) => {
        try {
          const { nome } = req.params;
          nome = NAOEXISTE.NOME;
          const db = new Database();
          return reply(await db.findOne(nome));
        } catch (error) {
          reply('DEU RUIM!').statusCode = 500;
        }
      },
      validate: {
        params: {
          nome: Joi.string()
            .max(10)
            .min(2)
            .required(),
        },
      },
    },
  },
]);
app.start(() => console.log('servidor no ar!!'));
// const http = require('http');
// http
//   .createServer((req, resp) => {
//     resp.end('Hello world');
//   })
//   .listen(3000, () => {
//     console.log('servidor está ativo!!');
//   });
