/*
    usamos o comando "use MEUBANCO", 
    ele criou autoMAGICAMENTE a base de dados
    usamos o comando "show collections" para visualizar
    as collections 
    usamos o "db.usuarios.find()" para mostrar 
    todos os usuarios
    
    usamos o "db.usuarios.insert({nome: "Erick",
                                    idade: 22})"

    para fazer o update usamos os seguintes comandos
    usamos o comando "$set" para falar quem será alterado
    em nosso objeto

    db.usuarios.update({"_id": 
        ObjectId("5a317658d16af54f1d8c19b0")}, 
        {$set: {nomzzzze: "Wendel"}})

    db.usuarios.remove({"_id": 
            ObjectId("5a317658d16af54f1d8c19b0")})    

*/

const Mongoose = require('mongoose');
/*
    Importamos o Mongoose, para manipular e validar os documentos
    do mongodb. Conectamos a mesma string de conexao anterior
*/
Mongoose.connect('mongodb://localhost/bootcamp');
//guardamos a variavel de conexão do mongo, para receber as mensagens
const connection = Mongoose.connection;
//Quando o mongo conectar, mostra as mensanges de sucesso ou error
connection.once('open', () => console.log('banco conectado'));
connection.once('error', error => console.error('DEU RUIM', error));

// mapeamos os objetos que nossa coleção deve ter
const usuario = new Mongoose.Schema({
  nome: {
    type: String,
    required: true,
  },
  idade: {
    type: Number,
    required: true,
  },
  dataAtualizacao: {
    type: Date,
    default: new Date(),
  },
});
// mapeamos a interface de dados
// informamos qual é o nome da coleção no mongoDB que ele
//deverá utilizar

const model = Mongoose.model('usuarios', usuario);

class Database {
  save(nome, idade) {
    const usuarioModel = new model({ nome: nome, idade: idade });
    return usuarioModel.save();
  }
  list() {
    return model.find();
  }
  //recebemos o id do usuario,
  // e os dados que precisamos para cadastrar

  update(id, { nome, idade }) {
    return model.update(
      { _id: id },
      //passamos a chave $set, para informar, quem será
      // alterado
      { $set: { nome: nome, idade: idade } },
    );
  }

  delete(id) {
    return model.remove({ _id: id });
  }

  findOne(nome) {
    return model.findOne({ nome: nome });
  }
}

//exportamos a nossa classe
module.exports = Database;

//criamos uma classe  para manipular os dados

// const database = new Database();
//chamamos a funcao save de Database
// database.save('Larissa', 40)
//   // quando terminar de cadastrar um usuario,
//   // deve listar os resultados
//   .then(resultado => database.list())
//   .then(resultado =>
//     database.update(resultado[0]._id, { nome: 'Xuxa', idade: 45 }),
//   )
//   .then(resultado => console.log(resultado));

// database.findOne('Larissa')
// .then(resultado => database.delete(resultado._id));

// instalamos o "nodemon" para facilitar
// o desenvolvimento

// const Mongojs = require('mongojs');
// const db = Mongojs('mongodb://localhost/bootcamp',
// ['usuarios']);

// // db.once('open', console.log('banco conectado'));

// db.usuarios.find({}, (err, docs) => {
//   console.log('docs', docs);
//   process.exit(0);
// });
